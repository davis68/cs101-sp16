# http://www.engineeringtoolbox.com/thermal-expansion-metals-d_859.html
'''
\begin{align*}
\Delta L & = \alpha \left( T - T_0 \right) \\
y & = mx + b \\
\Delta L & = \alpha T + \left( - \alpha T_0 \right)
\end{align}
'''

def thermex(T):
    '''
    Calculate linear thermal expansion of a material.
    
    T       temperature, deg F
    '''
    DL = 0.0000104 * (T-75)
    return DL


def thermex(T, alpha, T0=75):
    '''
    Calculate linear thermal expansion of a material.
    
    inputs:
    T       # temperature, deg F
    alpha   # linear expansion coefficient, 1/deg F
    T0      # temperature, deg F
    
    output:
    DL      percent change in length of object, in
    '''
    DL    = alpha * (T-T0)  # linear expansion, in
    return DL



import numpy as np
temps    = np.linspace(-25, 150, 8)
length   = 12.0  # in
ref_temp = 100  # deg F
alpha    = {'brass':  10.4e-6,
            'copper':  9.8e-6,
            'iron':    6.8e-6}  # 1/deg F

import matplotlib.pyplot as plt
%matplotlib inline

DLs = thermex(temps, alpha=alpha['copper'], T0=ref_temp)

plt.plot(temps, DLs, 'ro')
