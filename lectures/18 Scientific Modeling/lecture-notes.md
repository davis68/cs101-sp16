#   SCIENTIFIC MODELING  [13h00]

I'm Neal Davis.

For today's quiz, let's review NumPy.

1.  [13h01] (There is also another function:  `np.ones((3))`)

2.  [13h04]

3.  [13h07]

4.  [13h10]

5.  [13h12] `[1][2]` and `[1,2]` are both valid ways to index an array.  Why?


##  WHAT MODELING IS  [13h15]

Not *that* kind of modeling...

Our goal today is to formalize your understanding of scientific modeling.

You all have some informal idea of what is involved in scientific modeling from media reports and exposure to research and development processes.
Those of you who have had some engineering classes under your belt already will have a structured notion of what modeling means; others will not have seen something this structured before.

Before we get too deep, we need to define terms.

There are a lot of things we could correctly call models:
-   your mental model of the physics
-   the scientific models you build or use in applied assignments
-   your mental model of the computer, in this case of Python—yes, it is a model and you are often confused or surprised when things don't behave according to what you think you're telling the machine to do

When we say "model" in this class, we mean the second case:  creating a scientific model with an application.  This model can be represented by words, mathematics, or code.  Significantly, it may be accurate or inaccurate, or both across different domains of application.

Science actually runs this view of modeling backwards and forwards.

Sometimes we model backward—that is, we have an observation (a result) and we want to build a model which explains it.

Other times we model forward—we have an input and a known model, and we need to make useful predictions.

It's a gross oversimplification to say that the first is *science* and the second is *engineering*, but you get the idea.

-   How is this different from an operator?
    
    Interpretation.  An operator isn’t interpreted as a prediction.


##  THE MODEL LIFECYCLE  [13h18]

Briefly, you have a problem which motivates you to create a model.  You may need to design a bridge or a rocket, or you want to predict the weather.

You need to decide what physics and mathematics affect your problem, and which ones matter as well.  For instance, wind shear matters more if you are designing a skyscraper than if you are building a Ferris wheel.  Why?  (surface area)

This is the stage at which you determine features like the accuracy of the model.  If you want to implement a model to simulate the game *Super Mario Brothers*, you have to decide at which level you want to proceed.
-   You could have a simple version which simply tries to reproduce the basic behavior of the game console.
-   You could have a hardware-accurate simulation which reproduces quirks of the console, like known memory bugs.
-   You could try to simulate the underlying electronic circuits which give rise to the hardware behavior.
-   You could simulate the electrons moving among atoms in the silicon circuit boards to generate all the rest.  We understand these processes well—but, unfortunately, it's far beyond our current computing power.

So what level of accuracy do you need?  Sometimes you don't know until after you build a simple model and test it.

Next, you have to calibrate your model by verifying its behavior against known cases called benchmarks or unit tests.  You've actually been doing this every week in the labs—the assert blocks that test your code are really these unit tests, because they test each unit of your code to make sure it behaves according to spec.

The major stage (hopefully) is to apply your model, again and again, to make useful predictions that inform choices and guide research and development.  This is the mature phase of most models and software programs.

But sometimes you find that your model is inadequate or inaccurate, so you have to extend it beyond its original application.


##  PROBLEM STATEMENT  [13h21]

So like many of you probably did, I grew up in an old house with a lot of character.  In particular, at night the house would groan and settle due to thermal contraction and expansion.  Thermal expansion turns out to be a well-understood phenomenon that we can characterize precisely for many materials.  Metallurgy is, after all, a few thousand years old and fairly sophisticated.

So if we have a batch of brass rods from the same process, we can safely assume that they behave similarly—similar tensile strength, bending strength, and in particular similar thermal expansivity.  The rods will expand or contract to the same extent when heated or cooled to the same temperature.

So let's take this as our problem:  to compose a function which predicts the change in length due to change in temperature.  Let's suppose it's part of an engineering system that will be subject to reasonable thermal stresses—piping, for instance, must be designed with this behavior in mind.


##  MODEL DEFINITION  [13h24]

So what do we need to know to accomplish this?  We require an equation or relation of some kind from experimental observation or theory.

We have a full mathematical expression, which is a bit sticky but really not too awful.  Let's rephrase it in a familiar form so you can see how the relationship behaves.

We will implement as a Python function, of course, since that's what this class is about.

So let's do that really quick.  (thermex, part one)  [13h25]
(I want to edit code in front of you, so I'm going to use Jupyter.)


##  CALIBRATION  [13h29]

Recall that calibration is a simple test against reference points, for instance to correct the value of the thermal expansion coefficient $/alpha$, or to get the acceleration due to gravity $g$ correct.

Verification attempts to answer the question of whether our computer implementation match our mathematical model?  (This is a unit test against calculated values.)

Validation considers whether our computer implementation matches reality.  (This is a unit test against experimental values.)


##  APPLICATION  [13h31]

The solution involves stringing up your model in some way that it can actually make predictions.  (We prefer, of course, that these predictions be testable, at least at first.)  This is often called the main loop, since it frequently involves solving an array of equations for every point in a mesh.  (This is by far the most frequent thing most of you will do as scientists and engineers if you stay on the computational side of things—structural mechanics, fluid dynamics, electromagnetics, numerical relativity, and so on.  We may or may not get to this in the homework and labs.)

In this case, we will use a 1D array to explore a range of temperatures.

Analysis involves outputting results which are usable either directly or by another program to turn raw data into human-usable information.  Frequently this involves plotting and statistics.

An error estimate can be derived from experiment and/or theory—we'll forgo it this time in the interest of time, but we'll discuss sources of error in a moment.  (Of course, we mean error in the sense of "deviation from the true value" rather than "computer bug".)  In this week's lab, you derive error estimates against the most accurate of the three formulæ you use.

OK, let's switch back to Python and apply our model.  (thermex array, plotting)  [13h33]


##  EXTENSION  [13h35]

Finally, we reach the limit of our function's ability (in one way or another).

### shortcomings:  [known unknowns] what physics are missing?

sometimes this comes from pushing the model past its range of application

- melting
- nonlinear behavior:  coefficient of thermal expansion $/alpha$ is a function of temperature
- other materials

### surprises:  [unknown unknowns]

this comes about when a simulation predicts something unusual that is then confirmed by experiment or theory—relativity in 1919, gravity waves/LIGO
(find “stars not where they seemed”)

what about...

- wood (anisotropicity)?

A second version of this function may look like this.  (thermex part two)  [13h40]

Note that this passes the same unit tests that the first one should have (except that the required parameter list may have changed).  But it will also pass newer, more stringent tests.

Next time we'll move from scientific modeling to scientific programming.  We'll see you on Monday.  [13h48+]