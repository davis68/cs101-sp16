https://jupyter.readthedocs.org/en/latest/system.html
https://jupyter.readthedocs.org/en/latest/data_science.html

custom.js can be used to inject Javascript (d3.js, node.js, etc.)
~/.jupyter/jupyter_notebook_config.py
JUPYTER_CONFIG_DIR env can be used if ~/.jupyter is not desired.
JUPYTER_DATA_DIR for data files (default ~/.local/share/jupyter)

no more profiles:
JUPYTER_CONFIG_DIR=./jupyter_config jupyter notebook

Notebook extensions are installed with
jupyter nbextension install [--user] EXTENSION

Kernels are programming language specific processes that run independently and interact with the Jupyter Applications and their user interfaces.
jupyter kernelspec install [--user] KERNEL
https://github.com/ipython/ipython/wiki/IPython-kernels-for-other-languages

tools:
nbconvert
nbformat
nbgrader
jupyterhub
dockerspawner
tmpnb


https://jupyter-notebook.readthedocs.org/en/latest/

<div class="alert alert-warning"></div>
<div class="alert alert-success"></div>
<div class="alert alert-danger"></div>
<div class="alert alert-info"></div>
Caution
Important


<div class="admonition caution"></div>

<div class = "progress progress-striped active">
   <div class = "progress-bar progress-bar-success" role = "progressbar" 
      aria-valuenow = "60" aria-valuemin = "0" aria-valuemax = "100" style = "width: 40%;">
      
      <span class = "sr-only">40% Complete</span>
   </div>
</div>

%matplotlib inline etc. as defaults
from IPython.display import Audio
# auralize NumPy arrays or play wav/mp3 files
from IPython.display import YouTubeVideo
YouTubeVideo('sjfsUzECqK0')
from IPython.display import HTML
from base64 import b64encode
video = open("../images/animation.m4v", "rb").read()
video_encoded = b64encode(video).decode('ascii')
video_tag = '<video controls alt="test" src="data:video/x-m4v;base64,{0}">'.format(video_encoded)
HTML(data=video_tag)

from IPython.display import IFrame
IFrame('http://jupyter.org', width='100%', height=350)

https://nbviewer.jupyter.org/github/ipython/ipython/blob/3.x/examples/IPython%20Kernel/Rich%20Output.ipynb

MathJax can have more packages enabled

set notebooks to "trusted" to allow rich content
jupyter trust mynotebook.ipynb [other notebooks.ipynb]

We recommend using only one markdown header in a cell and limit the cell’s content to the header text. For flexibility of text format conversion, we suggest placing additional text in the next notebook cell.

101:  Command v. Edit modes

https://github.com/jupyter/docker-stacks
https://github.com/jupyter/jupyterhub
https://github.com/jupyter/jupyter
https://github.com/jupyter/dockerspawner

Config file and command line options
jupyter notebook --generate-config
https://jupyter-notebook.readthedocs.org/en/latest/config.html

https://jupyter-notebook.readthedocs.org/en/latest/public_server.html


Under the hood, Jupyter will persist the preferred configuration settings in ~/.jupyter/nbconfig/section.json, with section taking various value depending on the page where the configuration is issued. section can take various values like notebook, tree, and editor. A common section contains configuration settings shared by all pages.


———

X Set up Apache Server

/ Set up Jupyterhub, attach to Apache reverse proxy (or nginx)

/ Install nbgrader, scipy stack, MPL, etc.

~ Hook up the pieces

· Test with stormhold/tristan/yvaine users

    pip install nbgrader
    nbgrader extension install --symlink
    nbgrader extension activate


By default, nbgrader assumes that your assignments will be organized with the following directory structure:

{course_directory}/{nbgrader_step}/{student_id}/{assignment_id}/{notebook_id}.ipynb

cs101-sp16/release/davis68/lab6/lab6.ipynb

flags.update({
    'id': (
        {'SubmitApp' : {'update': True}},
        "Use a different username than that submitted."
    ),
})
# if this occurs, echo the true username to the timestamp file as a provenance

nbgrader assign lab6
nbgrader release lab6
    nbgrader fetch lab6
    nbgrader submit lab6
nbgrader collect lab6
nbgrader autograde lab6
nbgrader feedback lab6

add:
    nbgrader submit lab6 --netid=XXX # override user id
nbgrader report lab6 # sends feedback to email address of record
                     # or at least notice

# add Python2 to parallel mode
https://stackoverflow.com/questions/29773954/change-ipython-3-for-python-3-kernel-to-python2-for-the-cluster-too/29798906#29798906

    # remove an existing database
    import os
    if os.path.exists("gradebook.db"):
        os.remove("gradebook.db")
    
    # create a connection to the db using the nbgrader API
    from nbgrader.api import Gradebook
    gb = Gradebook("sqlite:///gradebook.db")
    
    # create an assignment
    gb.add_assignment("lab1", duedate="2016-02-01 15:00:00.000000 PST")
    
    # create a student
    gb.add_student("Bitdiddle", first_name="Ben", last_name="Bitdiddle")
    gb.add_student("Hacker", first_name="Alyssa", last_name="Hacker")
    gb.add_student("Reasoner", first_name="Louis", last_name="Reasoner")
    
    # send the assignment out
    !nbgrader assign "lab1" #--IncludeHeaderFooter.header=source/header.ipynb


———
# New AWS AMI
Amazon Linux AMI 2015.09.1 (PV) - ami-5fb8c835
m3.large
IAM:  admin
Auto-assign Public IP enable
32 GB magnetic
SSH, HTTPS, 8888 -> 0.0.0.0/0

# access refresher: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AccessingInstancesLinux.html
# setup guide: https://gist.github.com/iamatypeofwalrus/5183133
needs sufficient space -> 20 GB min!

chmod 400 ~/Downloads/cs101-test.pem
ssh -i ~/Downloads/cs101-test.pem -Y ec2-user@ec2-52-91-187-231.compute-1.amazonaws.com
ssh -i ~/Downloads/cs101-sp16-aws.pem -Y ec2-user@ec2-52-91-187-231.compute-1.amazonaws.com
    sudo su
    yum -y update
    yum -y groupinstall 'Development Tools'
    yum -y install git gcc gcc-c++ xauth tree npm
    wget https://3230d63b5fc54e62148e-c95ac804525aac4b6dba79b00b39d1d3.ssl.cf1.rackcdn.com/Anaconda2-2.4.1-Linux-x86_64.sh
    bash Anaconda2-2.4.1-Linux-x86_64.sh
        # -> /etc/anaconda2
    wget https://3230d63b5fc54e62148e-c95ac804525aac4b6dba79b00b39d1d3.ssl.cf1.rackcdn.com/Anaconda3-2.4.1-Linux-x86_64.sh
    bash Anaconda3-2.4.1-Linux-x86_64.sh
        # -> /etc/anaconda3
    source /root/.bashrc
    conda install jupyter
    
    # use the right pip!
    # could fix $PATH
    /etc/anaconda3/bin/pip install --upgrade pip
    /etc/anaconda3/bin/pip install jupyterhub
    
    # fix /etc/bashrc
    vi /etc/profile.d/setup101.sh
        #!/usr/bin/bash
        export PATH=/usr/local/bin:$PATH
        export PATH=/etc/anaconda3/bin:/etc/anaconda/bin:$PATH
        nbgrader extension activate assignment_list 2> /dev/null
        nbgrader extension deactivate create_assignment 2> /dev/null
    
    # install node/npm for jupyterhub
    git clone https://github.com/nodejs/node.git
    #export PATH="/etc/anaconda2/bin:$PATH"
    cd node/ ; ./configure
    make # takes a while
    make install
    /usr/local/bin/npm install -g configurable-http-proxy
    export PATH="/usr/local/bin:$PATH"
    export LD_LIBRARY_PATH="/usr/local/lib:$LD_LIBRARY_PATH"
    
    # create a temporary self-signed certificate (XXX:fix this later)
    mkdir certificates
    cd certificates/
    openssl req -x509 -nodes -days 365 -newkey rsa:1024 -keyout mycert.pem -out mycert.pem
    # and fix your config file per iamatypeofwalrus
    
    # get jupyterhub up and running now
    cd
    mkdir -p /etc/jupyterhub
    jupyterhub --generate-config -f /etc/jupyterhub/jupyterhub_config.py
    # any necessary editing:
    '''
    c.JupyterHub.port = 443
    c.JupyterHub.hub_port = 8888
    #c.JupyterHub.hub_ip = '*' # use :443 instead
    c.JupyterHub.ip = '172.31.14.120' # from AWS, private IP
    c.LocalAuthenticator.create_system_users = True
    c.Spawner.notebook_dir = '~/cs101'
    then to http://ec2-54-93-86-140.eu-central-1.compute.amazonaws.com:8888
    '''
    
    # also add admin users
    adduser stormhold
    passwd stormhold #Septimus
    echo "c.Authenticator.admin_users.append('stormhold')" >> /etc/jupyterhub/jupyterhub_config.py
    visudo
        stormhold ALL=(ALL) ALL
    
    jupyter notebook --generate-config
    vi ~/.jupyter/jupyter_notebook_config.py
        c.KernelSpecManager.whitelist = ['python2']
    cp -r /root/.jupyter /home/stormhold
    chown stormhold /home/stormhold/.jupyter -R
    
    jupyter notebook --ip="*"
    
    # add student users and JH, CS 101 directories
    adduser tristan
    passwd tristan #Septimus
    cp -r /home/stormhold/.jupyter /home/tristan/
    chown tristan /home/tristan/.jupyter/ -R
    mkdir -p /home/tristan/cs101
    cp cs101/nbgrader_config.py /home/tristan/cs101/
    chown tristan /home/tristan/cs101/nbgrader_config.py 
    
    adduser yvaine
    passwd yvaine #Septimus
    cp -r /home/stormhold/.jupyter /home/yvaine/
    chown yvaine /home/yvaine/.jupyter/ -R
    mkdir -p /home/yvaine/cs101
    cp cs101/nbgrader_config.py /home/yvaine/cs101/
    chown yvaine /home/yvaine/cs101/nbgrader_config.py 
    
    # add a python2 kernel
    mkdir -p /usr/local/share/jupyter/kernels/python2
    vi /usr/local/share/jupyter/kernels/python2/kernel.json
        {
         "argv": ["python2", "-m", "IPython.kernel",
                  "-f", "{connection_file}"],
         "display_name": "Python 2",
         "language": "python2"
        }
    
    # now set up nbgrader:
    /etc/anaconda3/bin/pip install nbgrader --upgrade
    nbgrader extension install --symlink --debug
    mkdir -p /class/cs101/tmp/exchange ; chmod 777 /class/cs101/tmp/exchange
    mkdir cs101; cd cs101/
    vi nbgrader_config.py
        c = get_config()
        c.NbGrader.course_id = "cs101"
        c.TransferApp.exchange_directory = '/tmp/exchange'
    nbgrader extension activate create_assignment
    vi /etc/jupyter/jupyter_notebook_config.py
        c.NotebookApp.reraise_server_extension_failures = True
        c.NotebookApp.extra_nbextensions_path = ['/usr/local/share/jupyter/nbextensions']
        c.NotebookApp.server_extensions.append('nbgrader.nbextensions')
        #c.NotebookApp.server_extensions = ['nbgrader.nbextensions']
    # also update ~/cs101/nbgrader_config.py and remove write perms
    
    ./setup_course.py # my script, if we have a CSV with students in it ready
    # add nbgrader_config.py to $HOME/cs101 for each user as well
    # and make it readonly
    
    nohup jupyterhub --config=/etc/jupyterhub/jupyterhub_config.py
    
    # for students:
    nbgrader extension install
    nbgrader extension activate                                                                                                       
    nbgrader extension deactivate create_assignment
    
    # this works to here
    export PATH="/usr/local/bin:$PATH" # for NPM if not in .bashrc
    jupyterhub --config=/etc/jupyterhub/jupyterhub_config.py
    
    # add DockerSpawner for SystemUserSpawner
    yum install docker docker-io
    git clone https://github.com/jupyter/dockerspawner
    cd dockerspawner
    pip install -r requirements.txt
    python setup.py install
    vi /etc/jupyterhub/jupyterhub_config.py
        c.JupyterHub.spawner_class = 'dockerspawner.SystemUserSpawner'
    git clone https://github.com/tianon/cgroupfs-mount.git
    source cgroupfs-mount/cgroupfs-mount
    service docker start
    cat /proc/cgroups
    docker pull jupyter/systemuser
    
    docker ps -a # or -l for last container
    docker logs 4452b7614b82
    # sudo ln -s /opt/conda/bin/python3 /usr/bin/python3
    '''
    Creating user yvaine (503)
    /usr/bin/env: python3: No such file or directory
    '''
    # Clear Docker containers
    docker rm $(docker ps -aq)
    # but should connect using NFS to home dir
    
    # to reset docker:
    yum clean all
    yum erase -y -v docker
    rm -rf /var/lib/docker
    yum install -y -v docker docker-io
    service docker start
    docker pull jupyter/systemuser
    
    # TODO:
    # - remove Py3
    # - add other kernels—MATLAB?
    #   - https://github.com/ipython/ipython/wiki/IPython-kernels-for-other-languages
    # - add DockerSpawner/Swarm as necessary
    jupyter --paths
    nbgrader --generate-config
    ifconfig # to double-check IP address
    # how does nbgrader handle updates w/ release?
    # try with --NbGraderApp.student_id=<Unicode>
    
    # useful extensions:
    jupyter nbextension install https://rawgit.com/jfbercher/latex_envs/master/latex_envs.zip  --user
    jupyter nbextension enable latex_envs/latex_envs
    https://github.com/damianavila/RISE
    
    # to change CSS etc.:
    tree /etc/anaconda3/share/jupyter/hub/
    vi /etc/anaconda3/share/jupyter/hub/templates/page.html # could be issue in installing nbgrader, but don't think so
    # https://github.com/jupyter/jupyterhub/issues/305 if so
    # https://github.com/jupyter/jupyterhub/issues/153
    
    
    # This works for Jupyter to have a single nbgrader notebook:
    vi /etc/jupyter/jupyter_notebook_config.py
        c.NotebookApp.ip = '*'
        c.NotebookApp.port = 8888
    jupyter notebook # navigate to http://ec2-54-93-86-140.eu-central-1.compute.amazonaws.com:8888/
    
    # to be a different user:
    su - stormhold
    
    # to monitor a file:
    git clone https://github.com/rvoicilas/inotify-tools.git
    cd inotify-tools
    ./autogen.sh
    ./configure --prefix=/usr && make && su -c 'make install'
    LD_LIBRARY_PATH=/usr/lib inotifywait -mr /etc/jupyter/jupyter_notebook_config.py
    /root/.local/share/jupyter/nbextensions/assignment_list/main.js
    /usr/local/share/jupyter/nbextensions/assignment_list/main.js
    

I have a fresh Jupyter/JupyterHub install sitting on AWS.  I have arranged it to have both the default Python3 and a Python2 kernel available.  This confirms that JupyterHub checks the `/usr/local/share/jupyter/kernels` directory.  However, JH will not recognize `nbgrader`, which naturally sits beside the `kernels/` directory in `nbextensions/`.  Jupyter does read `nbgrader`, but JH does not—what should I do to fix this situation?


[D 2015-12-10 19:34:06.677 stormhold log:47] 200 GET /user/stormhold/nbextensions/widgets/widgets/js/init.js?v=20151210193401 (128.174.11.30) 1.59ms

[D 2015-12-10 19:34:06.619 stormhold kernelspec:116] Found kernel python2 in /usr/local/share/jupyter/kernels
[D 2015-12-10 19:34:06.620 stormhold kernelspec:123] Native kernel (python3) available from /etc/anaconda3/lib/python3.5/site-packages/ipykernel/resources



stormhold:
nbgrader fetch lab1 --FetchApp.exchange_directory=/tmp/exchange --course=cs101


    vi /etc/jupyter/jupyter_notebook_config.py
        c.NotebookApp.reraise_server_extension_failures = True
        c.NotebookApp.extra_nbextensions_path = ['/usr/local/share/jupyter/nbextensions']
        c.NotebookApp.server_extensions.append('nbgrader.nbextensions')
        #c.NotebookApp.server_extensions = ['nbgrader.nbextensions']
        
        import os
        os.environ['JUPYTER_DATA_DIR'] = '/usr/local/share/jupyter/'
        c.Spawner.env_keep.append('JUPYTER_DATA_DIR')
        #c.NotebookApp.file_to_run = ''
        #c.MultiKernelManager.default_kernel_name = 'python2'
            IPython.notebook.config.update({
              "load_extensions": {"hello-scipy":true}
            })
        #c.NotebookApp.extra_nbextensions_path = ['/usr/local/share/jupyter/nbextensions']



————————

#*** https://zonca.github.io/2016/04/jupyterhub-sdsc-cloud.html
# https://github.com/jupyterhub/jupyterhub/blob/master/docs/source/troubleshooting.md
# https://github.com/jupyterhub/jupyterhub/issues/291

wget --no-check-certificate https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
 bash Miniconda3-latest-Linux-x86_64.sh
 ```

 use all defaults, answer "yes" to modify PATH

 ```
sudo apt-get install npm nodejs-legacy
sudo npm install -g configurable-http-proxy
conda install traitlets tornado jinja2 sqlalchemy 
pip install jupyterhub

yum install docker-enginer
service docker restart
docker pull jupyter/singleuser

————————
ssh -Y -l root sp16-cs101-001.studentspace.cs.illinois.edu  # can't see JH
ssh -Y -l root sp16-cs101-002.studentspace.cs.illinois.edu

https://cs101.studentspace.cs.illinois.edu/


conda update --prefix /etc/anaconda3/ anaconda
pip install pip-review versioneer
conda install -c https://conda.binstar.org/mwiebe dynd-python
conda install llvmlite
yum install libffi-devel hdf5-devel llvm-devel

pip-review --local --interactive
npm install -g configurable-http-proxy

wget https://raw.githubusercontent.com/compmodels/jupyterhub/master/swarmspawner.py
wget https://raw.githubusercontent.com/EdwardJKim/jupyterhub-info490/ef752e679faa8ebc94db0cc6a79d87ea87a113ac/roles/hub/files/remote_user_auth.py
cp ./*.py /etc/anaconda3/lib/python3.5/site-packages/

export HUB_IP="*"
export DOCKER_TLS_CERT=/home/deploy/jupyterhub-cs101/certificates/jupyterhub_host-cert.pem
export DOCKER_TLS_KEY=/home/deploy/jupyterhub-cs101/certificates/jupyterhub_host-key.pem
jupyterhub --config=/etc/jupyterhub/jupyterhub_config.py

# Linux chokes on adduser -q --gecos "" --disabled-password:
#        -q      Minimal user feedback.  In particular, the random password will
#	     not be echoed to standard output.

adduser --skel /skeleton_home --create-home -p

jupyterhub_config.py:
    c.LocalAuthenticator.create_system_users = True
    c.LocalAuthenticator.add_user_cmd = ['adduser', '--create-home']

/home/deploy/jupyterhub-cs101



# http://python.6.x6.nabble.com/Error-running-JupyterHub-td5075648.html
# https://github.com/uiuc-cse/jupyterhub-info490