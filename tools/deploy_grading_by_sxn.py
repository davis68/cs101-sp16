# deploy-grading-by-sxn.py assignment
#   assumes nbgrader default file/folder layout
#   should be run from tools/ folder

# configuration
import os
class_semester = 'cs101-sp16'
exchange_path = '/class/cs101/tmp/exchange/%s/grading'%(class_semester)
#exchange_path = '/Users/davis68/Courses/cs101-sp16/tmp'
if not os.path.isdir(exchange_path):
    print('%s path not found—check permissions.'%exchange_path)

import argparse

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('section', metavar='section', type=str, nargs=1,
                   help='the current section')
parser.add_argument('assignment', metavar='assignment', type=str, nargs=1,
                   help='the current assignment')

args = parser.parse_args()
sxn  = args.section[0]
asst = args.assignment[0]

'''
Fields in rpt_all_students.csv file:
    Net ID
    UIN
    Gender
    Last Name
    First Name
    Credit
    Level
    Year
    Subject
    Number
    Section
    CRN
    Degree Name
    Major 1 Name
    College
    Program Code
    Program Name
    FERPA
'''

# load list of students by section
import csv
students = {}
with open('../admin/rpt_all_students.csv') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        if row['Section'] != sxn:
            continue  # skip those not in this section
        students[row['Net ID']] = row['Section']

# send student content to TA by section
import os, subprocess, shutil
for key in students.keys():
    # find the current assignment in submitted/
    src_path = '../submitted/%s/%s'%(key,asst)
    if not os.path.isdir(src_path):
        print('%s\t%s\tsubmission not found'%(key, students[key]))
        continue
    # and copy it to the exchange directory grading/SXN/submitted/netid/labX
    # (this makes it work with nbgrader autograde and nbgrader formgrade)
    dest_path = os.path.join(exchange_path, students[key], 'submitted', key)
    dest_asst_path = os.path.join(dest_path, asst)
    if not os.path.isdir(dest_path):
        os.makedirs(dest_path)
    #print(dest_path)
    if os.path.isdir(dest_asst_path):
        shutil.rmtree(dest_asst_path)
    shutil.copytree(src_path, dest_asst_path)
    subprocess.call(['chgrp', '-R', 'cs101ta', dest_path])

