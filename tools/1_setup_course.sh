# set up grading directories
CLASS_SEMESTER="cs101-sp16"
cd /class/cs101/tmp/exchange/$CLASS_SEMESTER
mkdir -p grading
chgrp -R cs101ta /class/cs101/tmp/exchange/$CLASS_SEMESTER/grading
chmod -R ug+rwx /class/cs101/tmp/exchange/cs101-sp16/grading