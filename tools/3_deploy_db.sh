# set up grading directories
CLASS_SEMESTER="cs101-sp16"

# copy the generated gradebooks over to the grading directory
for sxn in AY{A..R}
do
    cp --force --backup=existing gradebook_$sxn.db /class/cs101/tmp/exchange/cs101-sp16/grading/$sxn/gradebook.db
    cp ../nbgrader_config.py /class/cs101/tmp/exchange/cs101-sp16/grading/$sxn
done

# set up rest of it
for section in AY{A..R}
do
    mkdir -p /class/cs101/tmp/exchange/$CLASS_SEMESTER/grading/$section/source
    cp -r /home/davis68/Courses/cs101-sp16/source/$1 /class/cs101/tmp/exchange/$CLASS_SEMESTER/grading/$section/source
    chgrp -R cs101ta $section
done

# run the "nbgrader assign labX' command in each
for sxn in AY{A..R}
do
    cd /class/cs101/tmp/exchange/cs101-sp16/grading/$sxn
    nbgrader assign $1 --force
done

chgrp -R cs101ta /class/cs101/tmp/exchange/$CLASS_SEMESTER/grading
chmod -R ug+rwx /class/cs101/tmp/exchange/$CLASS_SEMESTER/grading
