# send-feedback.py assignment
#   assumes nbgrader default file/folder layout inside of sections
#   should be run from tools/ folder

# configuration
import os
class_semester = 'cs101-sp16'
exchange_path = '/class/cs101/tmp/exchange/%s/grading'%(class_semester)
#exchange_path = '/Users/davis68/Courses/cs101-sp16/tmp'
if not os.path.isdir(exchange_path):
    print('%s path not found---check permissions.'%exchange_path)

import argparse

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('section', metavar='section', type=str, nargs=1,
                    help='the current assignment')
parser.add_argument('assignment', metavar='assignment', type=str, nargs=1,
                    help='the current assignment')

args = parser.parse_args()
asst = args.assignment[0]
sxn  = args.section[0]

# loop over each section
sxns = ['AYA', 'AYB', 'AYC', 'AYD', 'AYE', 'AYF', 'AYG', 'AYH', 'AYI', 'AYJ', 'AYK', 'AYL', 'AYM', 'AYN', 'AYO', 'AYP', 'AYQ', 'AYR']
if sxn not in sxns:
    raise Exception('Section %s not found.'%sxn)
tas = {}
tas['AYA'] = 'zzhng128'
tas['AYB'] = 'rghosh9'
tas['AYC'] = 'smshah4'
tas['AYD'] = 'delliso2'
tas['AYE'] = 'delliso2'
tas['AYF'] = 'natu2'
tas['AYG'] = 'smshah4'
tas['AYH'] = 'zzhng128'
tas['AYI'] = 'rghosh9'
tas['AYJ'] = 'nsharm11'
tas['AYK'] = 'ahketka2'
tas['AYL'] = 'jchen185'
tas['AYM'] = 'natu2'
tas['AYN'] = 'bhattad2'
tas['AYO'] = 'bhattad2'
tas['AYP'] = 'ahketka2'
tas['AYQ'] = 'jchen185'
tas['AYR'] = 'nsharm11'

import glob
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from os.path import basename

from random import randint
from time import sleep

# start SMTP server
s = smtplib.SMTP('localhost')

#for sxn in sxns:
sxn_path = os.path.join(exchange_path, sxn, 'feedback')
# get a list of students who were graded
students = os.listdir(sxn_path)
for student in students:
    student_path = os.path.join(sxn_path, student, asst)
    msg = MIMEMultipart()
    msg['Subject'] = '%s %s:  %s'%(class_semester, asst, student)
    msg['From'] = 'cs101@mx.illinois.edu'
    msg['To'] = '%s@illinois.edu'%(student)
    msg['Cc'] = 'cs101@mx.illinois.edu, %s@illinois.edu'%(tas[sxn])
    
    msg.attach(MIMEText('%s %s:  %s'%(class_semester, asst, student)))
    msg.attach(MIMEText('Do not reply to this email.  Contact your TA directly if you have questions.'))
    
    print('Sending email for %s'%student)
    
    # grab any html files in feedback/ and attach to email
    print('Attaching files:')
    htmls = glob.glob(os.path.join(student_path, '*.html'))
    for html in htmls:
        print('\t', html)
        with open(html, 'r', encoding='utf-8') as html_stream:
            msg.attach(MIMEApplication(html_stream.read(), Content_Disposition='attachment; filename="%s"'%basename(html), Name=basename(html)))
    
    # send email to student email address
    print(msg.items())
    sleep(randint(5,20))  # prevent DOS-style attack by randomly varying interval
    s.sendmail('%s@illinois.edu'%student, ['%s@illinois.edu'%student, 'cs101@mx.illinois.edu', '%s@illinois.edu'%(tas[sxn])], msg.as_string())

# close SMTP server
s.quit()
